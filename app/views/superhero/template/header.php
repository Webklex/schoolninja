<?php
/*
 * File: header.php
 * Category: -
 * Author: MSG
 * Created: 23.09.14 10:31
 * Updated: -
 *
 * Description:
 *  -
 */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=WEBSITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?=PAGE_ROOT?>templates/<?=$this->theme?>/css/bootstrap.css" media="screen">
    <link rel="stylesheet" href="<?=PAGE_ROOT?>templates/<?=$this->theme?>/css/bootswatch.min.css">
    <link rel="stylesheet" href="<?=PAGE_ROOT?>templates/<?=$this->theme?>/css/style.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="<?=PAGE_ROOT?>" class="navbar-brand"><?=WEBSITE_NAME?></a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <?php
            $navigation_path = ROOT_PATH.'/app/views/'.$this->theme.'/template/navigations/'.
                strtolower(($this->app->user->Extensions['Group']->name == ''?
                    'default':
                    $this->app->user->Extensions['Group']->name)).
                '.stp';
            require_once $navigation_path;
            ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>&nbsp;</h1>
        </div>
    </div>
    <div class="jumbotron">