<?php
/*
 * File: common.php
 * Category: Config
 * Author: MSG
 * Created: 23.09.14 10:19
 * Updated: -
 *
 * Description:
 *  -
 */

define('PAGE_ROOT', '/');
define('ROOT_PATH', '/var/www/dev/schoolninja');

define('WEBSITE_NAME', 'SchoolNinja');

define('DEBUG', true);
define('VERSION', '1.1411');
define('DEFAULT_TEMPLATE', 'superhero');

