<?php
 /*
 * File: Question.php
 * Category: -
 * Author: MSG
 * Created: 09.10.14 22:27
 * Updated: -
 *
 * Description:
 *  -
 */

class Category extends Model {

    static $validates_presence_of = array(array('name'));

    protected $relations = array(
        'has_many' => array(
            array('module', 'category_id', 'id')
        )
    );

}
?>