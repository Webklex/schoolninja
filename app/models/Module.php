<?php
 /*
 * File: Module.php
 * Category: -
 * Author: MSG
 * Created: 09.10.14 22:26
 * Updated: -
 *
 * Description:
 *  -
 */

class Module extends Model {

    static $validates_presence_of = array(array('name'));

    protected $relations = array(
        'has_many' => array(
            array('question','module_id', 'id')
        )
    );

}
?>
