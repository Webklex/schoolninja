<?php
/*
 * File: Model.php
 * Category: -
 * Author: MSG
 * Created: 13.10.14 08:13
 * Updated: -
 *
 * Description:
 *  -
 */

class Model extends ActiveRecord\Model{

    public $Extensions = array();
    protected $relations = array('has_one' => array(), 'belongs_to' => array(), 'has_many' => array());

    static $after_construct = array('set_relations');

    static $before_destroy = array('log_action_destroy');
    static $before_update = array('log_action_update');
    static $before_create = array('log_action_create');



    public function log_action_destroy(){
        if(isset($_GET['id']) || isset($_POST['id'])){

            $id = isset($_GET['id'])?$_GET['id']:$_POST['id'];

            Log::create(array(
                'user_id'       => $_SESSION['user_id'],
                'controller'    => $_GET['c'],
                'method'        => $_GET['m'],
                'action'        => json_encode(array('destroy' => array('id' => $id))),
                'created'       => time()
            ));
        }
    }
    public function log_action_update(){
        if(isset($_GET['id']) || isset($_POST['id'])){

            $id = isset($_GET['id'])?$_GET['id']:$_POST['id'];

            Log::create(array(
                'user_id'       => $_SESSION['user_id'],
                'controller'    => $_GET['c'],
                'method'        => $_GET['m'],
                'action'        => json_encode(array('update' => array('id' => $id))),
                'created'       => time()
            ));
        }
    }
    public function log_action_create(){
        if($this->table_name() !== 'logs'){
            Log::create(array(
                'user_id'       => $_SESSION['user_id'],
                'controller'    => $_GET['c'],
                'method'        => $_GET['m'],
                'action'        => json_encode(array('create' => array('id' => ($this->last()->id)+1))),
                'created'       => time()
            ));
        }
    }

    public function set_relations(){

        if(!empty($this->relations['has_one'])){
            foreach($this->relations['has_one'] as $relation){
                $upper =  ucfirst($relation);
                $lower = strtolower($relation);
                $id_table = $lower.'_id';

                if(class_exists($upper)){
                    $this->Extensions[$upper] = $upper::find($this->$id_table);
                }
            }
        }

        if(!empty($this->relations['belongs_to'])){
            foreach($this->relations['belongs_to'] as $relation){
                $model =  ucfirst($relation[0]);
                $id_table = $relation[1];
                $id = (empty($relation[2])?'id':$relation[2]);

                if(class_exists($model)){
                    $this->Extensions[$model] = $model::find(array('conditions' => array($id_table.' = ?', $this->$id)));
                }
            }
        }

        if(!empty($this->relations['has_many'])){
            foreach($this->relations['has_many'] as $relation){
                $model =  ucfirst($relation[0]);
                $id_table = $relation[1];
                $id = (empty($relation[2])?'id':$relation[2]);

                if(class_exists($model)){
                    $this->Extensions[$model] = $model::all(array('conditions' => array($id_table.' = ?', $this->$id)));
                }
            }
        }
    }
}