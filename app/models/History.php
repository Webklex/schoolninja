<?php
 /*
 * File: History.php
 * Category: -
 * Author: MSG
 * Created: 09.10.14 22:26
 * Updated: -
 *
 * Description:
 *  -
 */

class History extends Model {

    protected $relations = array(
        'belongs_to' => array(
            array('users', 'id', 'user_id'),
            array('questions', 'id', 'question_id')
        )
    );

}
?>
