<?php
 /*
 * File: Question.php
 * Category: -
 * Author: MSG
 * Created: 09.10.14 22:27
 * Updated: -
 *
 * Description:
 *  -
 */

class Question extends Model {

    static $validates_presence_of = array(array('question'));

    protected $relations = array(
        'belongs_to' => array(
            //array('module', 'id', 'module_id')
        ),
        'has_many' => array(
            array('answer', 'question_id', 'id')
        )
    );

}
?>