<?php
 /*
 * File: Answer.php
 * Category: -
 * Author: MSG
 * Created: 09.10.14 22:25
 * Updated: -
 *
 * Description:
 *  -
 */

class Answer extends Model {

    static $validates_presence_of = array(array('answer'));

    protected $relations = array(
        'has_many' => array(
            array('histories', 'answer_id', 'id'),
            array('users', 'user_id', 'id')
        ),
        'belongs_to' => array(
            array('questions', 'id', 'question_id')
        )
    );
}
?>