<?php
/*
 * File: User.php
 * Category: Model
 * Author: MSG
 * Created: 24.09.14 08:32
 * Updated: -
 *
 * Description:
 *  -
 */

class User extends Model{


    protected $relations = array(
        'belongs_to' => array(
            array('group', 'id', 'group_id'),
        )
    );

    static $validates_presence_of = array(
        array('name'),
        array('email'),
        array('passwd'),
        array('salt')
    );

    static $validates_uniqueness_of = array(
        array('name'),
        array('email')
    );
}
?>
