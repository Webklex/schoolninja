<?php
class QuestionController extends AppController {

    public function view() {
        return Answer::all(array('conditions' => array('question_id = ?', $this->data['get']['id'])));
    }

    public function create() {
        if(!empty($this->data['post']['question'])){
            try{
                $question = Question::create(array(
                    'question'         => $this->data['post']['question'],
                    'hint'  => $this->data['post']['hint'],
                    'module_id'  => $this->data['post']['id'],
                    'date_created' => time()
                ));

                if(count($question->errors->get_raw_errors()) > 0){
                    $this->setFlasher(array(
                        'type'      => 'danger',
                        'title'     => 'Ein Fehler ist aufgetreten',
                        'content'   => 'Hmmm... mit deinen Angaben konnten wir leider nichts anfangen. Bitte versuche es erneut mit anderen Daten.'
                    ));
                }else{
                    $this->router->redirect(array('controller' => 'module', 'method' => 'view', 'args' => array('id' => $this->data['post']['id'])));
                }
            }catch(Exception $e){
                var_dump($e);
                $this->setFlasher(array(
                    'type'      => 'danger',
                    'title'     => 'Ein Fehler ist aufgetreten',
                    'content'   => 'Hmmm... mit deinen Angaben konnten wir leider nichts anfangen. Bitte versuche es erneut mit den richtigen Daten.'
                ));
            }
        }
        return Module::find($this->data['get']['id']);
    }

    public function delete() {
        try{
            if($this->data['post']['id']){
                $question = Question::find($this->data['post']['id']);
                $module_id = $question->module_id;
                $question->delete();
                $this->router->redirect(array('controller' => 'module', 'method' => 'view', 'args' => array('id' => $module_id)));
            }
        }catch(Exception $e){
            $this->router->redirect(array('controller' => 'home', 'method' => 'error_404'));
        }

        return Question::find($this->data['get']['id']);
    }

    public function update() {
        if(isset($this->data['post']['id'])) {
            $question = Question::find($this->data['post']['id']);
            $params = $this->data['post'];
            unset($params['id']);
            $question->update_attributes($params);

            $this->setFlasher(array(
                'type'      => 'success',
                'title'     => 'Eintrag erfolgreich gespeichert',
                'content'   => 'Die vorgenommenen Änderungen wurden erfolgreich gespeichert.'
            ));
        }
        return Question::find($this->data['get']['id']);
    }
}
?>