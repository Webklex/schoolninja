<?php
class ModuleController extends AppController {

    public function index(){
        return Module::find('all');
    }
    
    public function view() {
        $module = Module::find($this->data['get']['id']);

        if($module){
            return $module;
        }else{
            $this->router->redirect(array('controller' => 'module', 'method' => 'index'));
        }
    }

    public function create() {
        if(!empty($this->data['post']['name'])){

            try{
                $module = Module::create(array(
                    'name'  => $this->data['post']['name'],
                    'description'      => $this->data['post']['description'],
                    'category_id'        => $this->data['get']['id'],
                    'date_created'     => time()
                ));

                if(count($module->errors->get_raw_errors()) > 0){
                    $this->setFlasher(array(
                        'type'      => 'danger',
                        'title'     => 'Ein Fehler ist aufgetreten',
                        'content'   => 'Hmmm... mit deinen Angaben konnten wir leider nichts anfangen. Bitte versuche es erneut mit anderen Daten.'
                    ));
                }else{
                    $this->router->redirect(array('controller' => 'category', 'method' => 'view', 'args' => array('id' => $this->data['get']['id'])));
                }
            }catch(Exception $e){
                var_dump($e);
                $this->setFlasher(array(
                    'type'      => 'danger',
                    'title'     => 'Ein Fehler ist aufgetreten',
                    'content'   => 'Hmmm... mit deinen Angaben konnten wir leider nichts anfangen. Bitte versuche es erneut mit den richtigen Daten.'
                ));
            }
        }
    }

    public function update() {
        if(isset($this->data['post']['id'])) {
            $module = Module::find($this->data['post']['id']);
            $params = $this->data['post'];
            unset($params['id']);
            $module->update_attributes($params);

            $this->setFlasher(array(
                'type'      => 'success',
                'title'     => 'Eintrag erfolgreich gespeichert',
                'content'   => 'Die vorgenommenen Änderungen wurden erfolgreich gespeichert.'
            ));
        }
        return Module::find($this->data['get']['id']);
    }

    public function delete() {
        try{
            if($this->data['post']['id']){
                $module = Module::find_by_id($this->data['post']['id']);
                $category_id = $module->category_id;
                $module->delete();
                $this->router->redirect(array('controller' => 'category', 'method' => 'view', 'args' => array('id' => $category_id)));
            }
        }catch(Exception $e){
            $this->router->redirect(array('controller' => 'home', 'method' => 'error_404'));
        }

        return Module::find_by_id($this->data['get']['id']);
    }

}
?>
