<?php
class CategoryController extends AppController {

    public function index(){
        return Category::find('all');
    }

    public function view() {
        try{
            $category = Category::find($_GET['id']);

            if($category->id > 0){

                return $category;
            }else{
                $this->router->redirect(array('controller' => 'category', 'method' => 'index'));
            }

        }catch(Exception $e){
            $this->router->redirect(array('controller' => 'home', 'method' => 'error_404'));
        }
    }

    public function create() {
        if(!empty($this->data['post'])){

            try{
                $category = Category::create(array(
                    'name'  => $this->data['post']['name'],
                    'description'      => $this->data['post']['description'],
                    'date_created'     => time()
                ));

                if(count($category->errors->get_raw_errors()) > 0){
                    $this->setFlasher(array(
                        'type'      => 'danger',
                        'title'     => 'Ein Fehler ist aufgetreten',
                        'content'   => 'Hmmm... mit deinen Angaben konnten wir leider nichts anfangen. Bitte versuche es erneut mit anderen Daten.'
                    ));
                }else{
                    $this->router->redirect(array('controller' => 'category', 'method' => 'index'));
                }
            }catch(Exception $e){
                $this->setFlasher(array(
                    'type'      => 'danger',
                    'title'     => 'Ein Fehler ist aufgetreten',
                    'content'   => 'Hmmm... mit deinen Angaben konnten wir leider nichts anfangen. Bitte versuche es erneut mit den richtigen Daten.'
                ));
            }
        }
    }

    public function update() {
        if(isset($this->data['post']['id'])) {
            $category = Category::find($this->data['post']['id']);
            $params = $this->data['post'];
            unset($params['id']);
            $category->update_attributes($params);

            $this->setFlasher(array(
                'type'      => 'success',
                'title'     => 'Eintrag erfolgreich gespeichert',
                'content'   => 'Die vorgenommenen Änderungen wurden erfolgreich gespeichert.'
            ));
        }
        return Category::find($this->data['get']['id']);
    }

    public function delete() {
        try{
            if($this->data['post']['id']){
                $category = Category::find_by_id($this->data['post']['id']);
                $category->delete();
                $this->router->redirect(array('controller' => 'category', 'method' => 'index'));
            }
        }catch(Exception $e){
            $this->router->redirect(array('controller' => 'home', 'method' => 'error_404'));
        }

        return Category::find_by_id($this->data['get']['id']);
    }

}
?>
