<?php
 /*
 * File: AnswerController.php
 * Category: -
 * Author: MSG
 * Created: 09.10.14 22:20
 * Updated: -
 *
 * Description:
 *  -
 */

  class AnswerController extends AppController{

      public function create(){
          if(!empty($this->data['post']['answer'])) {
              try{
                  $answer = Answer::create(array(
                      'answer'  => $this->data['post']['answer'],
                      'kind'      => $this->data['post']['kind'],
                      'question_id' => $this->data['post']['id'],
                      'date_created'     => time()
                  ));

                  if(count($answer->errors->get_raw_errors()) > 0){
                      $this->setFlasher(array(
                          'type'      => 'danger',
                          'title'     => 'Ein Fehler ist aufgetreten',
                          'content'   => 'Hmmm... mit deinen Angaben konnten wir leider nichts anfangen. Bitte versuche es erneut mit anderen Daten.'
                      ));
                  }else{
                      $this->router->redirect(array('controller' => 'question', 'method' => 'view', 'args' => array('id'=> $this->data['post']['id'])));
                  }
              }catch(Exception $e){
                  var_dump($e);
                  $this->setFlasher(array(
                      'type'      => 'danger',
                      'title'     => 'Ein Fehler ist aufgetreten',
                      'content'   => 'Hmmm... mit deinen Angaben konnten wir leider nichts anfangen. Bitte versuche es erneut mit den richtigen Daten.'
                  ));
              }
          }
          return Question::find($this->data['get']['id']);
      }

      public function update() {
          if(!empty($this->data['post'])) {
              $params = $this->data['post'];
              $answer = Answer::find($params['id']);
              unset($params['id']);
              $answer->update_attributes($params);

              $this->setFlasher(array(
                  'type'      => 'success',
                  'title'     => 'Eintrag erfolgreich gespeichert',
                  'content'   => 'Die vorgenommenen Änderungen wurden erfolgreich gespeichert.'
              ));
          }
          return  Answer::find($this->data['get']['id']);
      }


      public function delete() {
          try{
              if($this->data['post']['id']){
                  $answer = Answer::find($this->data['post']['id']);
                  $question_id = $answer->question_id;
                  $answer->delete();
                  $this->router->redirect(array('controller' => 'question', 'method' => 'view', 'args' => array('id' => $question_id)));
              }
          }catch(Exception $e){
              $this->router->redirect(array('controller' => 'home', 'method' => 'error_404'));
          }

          return Answer::find($this->data['get']['id']);
      }
  }

?>