<?php
/*
 * File: install.php
 * Category: -
 * Author: MSG
 * Created: 24.09.14 08:28
 * Updated: -
 *
 * Description:
 *  -
 */


$start = microtime(true);
require_once __DIR__.'/../core/CoreController.php';
$core = new CoreController();

$db = new mysqli(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);

$sql = '';
$errors = false;
$lines = file(__DIR__.'/../sql/database.sql');

try{
    foreach ($lines as $line){
        // Skip it if it's a comment
        if (substr($line, 0, 2) == '--' || $line == ''){
            continue;
        }

        // Add this line to the current segment
        $sql .= $line;
        // If it has a semicolon at the end, it's the end of the query
        if (substr(trim($line), -1, 1) == ';'){
            // Perform the query
            $sql = str_replace('[%DB%]', DATABASE_NAME, $sql);

            $db->query($sql) or $errors[] = array($db->error, $sql);
            // Reset temp variable to empty
            $sql = '';
        }
    }
    $success[] = 'Die Datenbank wurde erfolgreich installiert';
}catch(Exception $e){
    $errors[] = array('Die Datenbank konnte nicht installiert werden',$e->getMessage());
}

echo "\n-----------------------------------------";
if($errors == false){
    echo "\n\tInstallation abgeschlossen\n";
    echo "\t      +OK: ".number_format((microtime(true)-$start),2).' sec.';
}else{
    echo "\n\tInstallation abgebrochen\n";
    echo "\t+ERROR: ".number_format((microtime(true)-$start),2).' sec.'."\n";

    foreach($errors as $error){
        echo "\n\t################################";
        echo "\n\tERROR:\n\t".str_replace("\n","\n\t",$error[0]);
        echo "\n\t--------------------------------";
        echo "\n\tDESCRIPTION:\n\t".str_replace("\n","\n\t",$error[1])."";
        echo "################################\n";
    }
}
echo "\n-----------------------------------------\n\n";
