<?php
/*
 * File: starter.php
 * Category: Testing
 * Author: MSG
 * Created: 14.11.14 06:41
 * Updated: -
 *
 * Description:
 *  -
 */
$start = microtime(true);

require_once '../app/core/CoreController.php';

$reports = array();

$core = new CoreController();




/*Version control*/
$startTimer = microtime(true);
if(VERSION !== '1.1411'){
    $reports[] = array('VERSION CONFLICT', 'Aktuelle Version ist 1.1411 nicht '.VERSION);
}else{
    $reports[] = array('VERSION CONTROLL', 'OK+'.(microtime(true)-$startTimer).' sec');
}



/*Modelchecks*/
$startTimer = microtime(true);
$models = array('User', 'Answer', 'Category', 'Monser');
foreach($models as $model){
    try{
        if(class_exists($model)){
            if(count($model::find('all')) == 0){
                $reports[] = array('MODEL CHECK - '.$model, 'Model enthält keine Daten');
            }
        }else{
            $reports[] = array('MODEL CHECK - '.$model, 'Model existiert nicht');
        }
    }catch(Exception $e){
        $reports[] = array('MODEL CHECK - '.$model, 'Model existiert nicht');
    }
}
$reports[] = array('MODEL CHECK', 'OK+'.(microtime(true)-$startTimer).' sec');






/*Ausgabe*/
echo "
    \n\t######################TESTRESULTS######################
    \tDuration: ".(microtime(true)-$start)." sec
    \tDate: ".date('d.m.Y H:i')."
    \t#######################################################

    ";

foreach($reports as $su){
    echo "\t----".$su[0]."
    \t".$su[1]."

    ";
}